/*********************

Station météo connectée

Affiche la temperature ou hygrometrie ou heat index sur l'afficheur LCD
Les boutons haut/bas permettent de choisir la variable a afficher
Le bouton droit envoie les valeurs sur le serveur

----------------------------------------------
Branchements :

Sonde shield DHT :
Pin Arduino D4 sur pin D4 DHT
Pin Arduino 3.3V sur 3.3V DHT
Pin Arduino GND sur GND DHT

// Notes du constructeur
// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect of the sensor to whatever your DHTPIN is
// Connect pin of the sensor to GROUND
// Connect a 10K resistor from pin (data) to pin (power) of the sensor

Afficheur LCD :
Pin arduino D1 sur Pin SDA afficheur (datas)
Pin arduino D2 sur Pin SCL afficheur (clock)
Pin Arduino 5V sur 5V afficheur
Pin Arduino GND sur GND afficheur

**********************/

#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <SPI.h>

#include "DHT.h"

// CONFIG SPI MCP3008

#define slaveSelect D8

SPISettings MCP3008(2000000, MSBFIRST, SPI_MODE0);

const int  CS_MCP3008       = 8;          // ADC Chip Select
const byte adc_single_ch0   = (0x08);     // ADC Single Channel 0
const byte adc_single_ch1   = (0x09);     // ADC Single Channel 1
const byte adc_single_ch2   = (0x0A);     // ADC Single Channel 2
const byte adc_single_ch3   = (0x0B);     // ADC Single Channel 3
const byte adc_single_ch4   = (0x0C);     // ADC Single Channel 4
const byte adc_single_ch5   = (0x0D);     // ADC Single Channel 5
const byte adc_single_ch6   = (0x0E);     // ADC Single Channel 6
const byte adc_single_ch7   = (0x0F);     // ADC Single Channel 7

// CONFIGURATION DU DHT

#define DHTPIN D4     // Pin arduino D4 sur le DHT
#define RELAIS1PIN D3     // Pin arduino D3 sur le relais 1

// Definition du type de DHT connecté
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

ESP8266WiFiMulti WiFiMulti;
ESP8266WebServer server(80);

// These #defines make it easy to set the backlight color
#define RED 0x1
#define GREEN 0x2
#define YELLOW 0x3
#define BLUE 0x4
#define VIOLET 0x5
#define TEAL 0x6
#define WHITE 0x7

uint8_t i_couleurCourante = 7;
String s_sablier = "*";

////////////////////////////////////////////////////////////////
// VARIABLES MODIFIABLES VIA L'INTERFACE WEB
////////////////////////////////////////////////////////////////

int i_frequenceMesuresMs = 5000;

// Variable courante ( affichée sur le LCD )
// 1 = temp ; 2 = hygro ; 3 = heat
int i_varCourante = 1;

bool b_mesureTemperatureDHT22 = true;
bool b_mesureTemperatureTMP36 = true;
bool b_mesureHeatIndex = true;
bool b_mesureHygrometrie = true;
bool b_mesureLuminosite = true;
bool b_autoRefreshHtml = true;

bool b_etatSortieRelais1 = false;

////////////////////////////////////////////////////////////////

// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();


// Types de variables
#define VAR_TEMP_DHT22 1
#define VAR_TEMP_TMP36 2
#define VAR_HYGRO 3
#define VAR_HEAT 4
#define VAR_LUMINOSITE 5

//#define VAR_TEMP_DHT11 6

// Valeurs des variables mesurees
float f_temperatureCourante_DHT22 = -1;
float f_temperatureCourante_TMP36 = -1;
float f_hygroCourante = -1;
float f_heatCourante = -1;
int photocellReading = 0; // the analog reading from the analog resistor divider

int photocellPin = 0; // the cell and 10K pulldown are connected to a0


int timeDebut = 0;
int timeFin = 0;




int adc_single_channel_read(byte readAddress)
{
 
  byte dataMSB =    0;
  byte dataLSB =    0;
  byte JUNK    = 0x00;
  
  SPI.beginTransaction (MCP3008);
  digitalWrite         (slaveSelect, LOW);
  SPI.transfer         (0x01);                                 // Start Bit
  dataMSB =            SPI.transfer(readAddress << 4) & 0x03;  // Send readAddress and receive MSB data, masked to two bits
  dataLSB =            SPI.transfer(JUNK);                     // Push junk data and get LSB byte return
  digitalWrite         (slaveSelect, HIGH);
  SPI.endTransaction   ();
 
  return               dataMSB << 8 | dataLSB;
 
}


//
// Mesure d'une variable ( temp, hygro, heat )
//
float mesureValeur(int _i_variable)
{
  switch ( _i_variable)
  {
    case VAR_TEMP_DHT22 :
    {
      // Read temperature as Celsius (the default)
      Serial.println("Lecture temperature Celsius ( DHT22 ) ...");
      f_temperatureCourante_DHT22 = dht.readTemperature();
      if (isnan(f_temperatureCourante_DHT22))
      {
        Serial.println("Erreur lecture de la temperature");
        return -1;
      }
      else
      {
        Serial.print("Temperature : ");
        Serial.print(f_temperatureCourante_DHT22);
        Serial.println(" *C");
        return f_temperatureCourante_DHT22;
      }   
    }   
    break;

    case VAR_TEMP_TMP36 :
    {
      Serial.println("Lecture temperature Celsius ( TMP36 ) ...");
      f_temperatureCourante_DHT22 = dht.readTemperature();
      int lectureSpiTemp = adc_single_channel_read (adc_single_ch0);
      Serial.println("Lecture SPI = " + String(lectureSpiTemp));
      double vRef        = 4.78;
      float milli = 1024;
      float voltage = (float(lectureSpiTemp) * float(vRef)) / float(1024);
       Serial.println("Voltage = " + String(voltage,2));
      f_temperatureCourante_TMP36 = (voltage - 0.47) * 100 ; //convertie de 10mv par degrés avec un décalage (offset) de 500 mV ( -0.5 )
      
      if (isnan(f_temperatureCourante_TMP36) || f_temperatureCourante_TMP36 == -1)
      {
        Serial.println("Erreur lecture de la temperature");
        return -1;
      }
      else
      {
        Serial.println("Temperature : " + String(f_temperatureCourante_TMP36,2)) + " *C";
        return f_temperatureCourante_TMP36;
      }   
    }   
    break;

    case VAR_HYGRO :
    {
      // Reading temperature or humidity takes about 250 milliseconds!
      // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
      Serial.println("Lecture hygrometrie ...");
      f_hygroCourante = dht.readHumidity();
      if (isnan(f_hygroCourante))
      {
        Serial.println("Erreur lecture de l'hygrometrie");
        return -1;
      }
      else
      {
        Serial.print("Hygrometrie : ");
        Serial.print(f_hygroCourante);
        Serial.println("%");
        return f_hygroCourante;
      }
    }
    break;
    
    case VAR_HEAT :
    {
      // Compute heat index in Celsius (isFahreheit = false)
      Serial.println("Calcul HeatIndex Celsius ...");
      f_heatCourante = dht.computeHeatIndex(f_temperatureCourante_DHT22, f_hygroCourante, false);
      
      if (isnan(f_heatCourante) && f_heatCourante != -1)
      {
        Serial.println("Erreur lecture du heat index");
        return -1;
      }
      else
      {
        Serial.print("Heat : ");
        Serial.print(f_heatCourante);
        Serial.println(" *C");
        return f_heatCourante;
      }
    }
    break;

    case VAR_LUMINOSITE :
    {
      Serial.println("Lecture luminosite ...");
      double vRef      = 4.78;
      //photocellReading = analogRead(photocellPin);
      photocellReading = adc_single_channel_read (adc_single_ch1);
      Serial.print("Lecture = " + String(photocellReading) + " - Volt : " + String( (photocellReading * vRef) / 1024, 2));
      
      if (photocellReading < 10) {
        Serial.println(" - Noir");
      } else if (photocellReading < 200) {
        Serial.println(" - Sombre");
      } else if (photocellReading < 500) {
        Serial.println(" - Lumiere");
      } else if (photocellReading < 800) {
        Serial.println(" - Lumineux");
      } else {
        Serial.println(" - Tres lumineux");
      }
      return photocellReading;
      
    }
    break;
  }
}



//
// Affiche la valeur d'une variable sur l'afficheur LCD
//
void afficheValeurLCD(int _i_variable)
{
  switch ( _i_variable)
  {
    case VAR_TEMP_DHT22 :
    {
      if (isnan(f_temperatureCourante_DHT22) || f_temperatureCourante_DHT22 == -1)
      {
        //lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Temperature 1");
        lcd.setCursor(0, 1);
        lcd.print("Erreur de lecture");
      }
      else
      {
        //lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Temperature 1");
        lcd.setCursor(0, 1);
        lcd.print(String(f_temperatureCourante_DHT22) + " *C");
      }   
    }   
    break;

    case VAR_TEMP_TMP36 :
    {
      if (isnan(f_temperatureCourante_TMP36) || f_temperatureCourante_TMP36 == -1)
      {
        //lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Temperature 2");
        lcd.setCursor(0, 1);
        lcd.print("Erreur de lecture");
      }
      else
      {
        //lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Temperature 2");
        lcd.setCursor(0, 1);
        lcd.print(String(f_temperatureCourante_TMP36) + " *C");
      }   
    }   
    break;

    case VAR_HYGRO :
    {
      if (isnan(f_hygroCourante) || f_hygroCourante == -1)
      {
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Hygrometrie :");
        lcd.setCursor(0, 1);
        lcd.print("Erreur de lecture");
      }
      else
      {
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Hygrometrie :");
        lcd.setCursor(0, 1);
        lcd.print(String(f_hygroCourante) + " %");
      }
    }
    break;
    
    case VAR_HEAT :
    {
      if (isnan(f_heatCourante) || f_heatCourante == -1)
      {
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Heat :");
        lcd.setCursor(0, 1);
        lcd.print("Erreur de lecture");
      }
      else
      {
        lcd.setCursor(0, 0);
        lcd.print(s_sablier + " Heat :");
        lcd.setCursor(0, 1);
        lcd.print(String(f_heatCourante) + " *C ");
      }
    }
    break;

    case VAR_LUMINOSITE :
    {
      String lbl_luminosite = "";
      if (photocellReading < 10) 
      {
        Serial.println(" - Noir");
        lbl_luminosite = "Noir       ";
      } 
      else if (photocellReading < 200) 
      {
        Serial.println(" - Sombre");
        lbl_luminosite = "Sombre     ";
      } 
      else if (photocellReading < 500) {
        Serial.println(" - Lumiere");
        lbl_luminosite = "Lumiere    ";
      } 
      else if (photocellReading < 800) 
      {
        Serial.println(" - Lumineux");
        lbl_luminosite = "Lumineux   ";
      } 
      else 
      {
        Serial.println(" - Tres lumineux");
        lbl_luminosite = "lumineux ++";
      }
      lcd.setCursor(0, 0);
      lcd.print(s_sablier + " Luminosite :");
      lcd.setCursor(0, 1);
      lcd.print(String(photocellReading) + " " + lbl_luminosite );
    }
    break;
  }
}


//
// Envoi sur le serveur des valeurs mesurees
//
bool envoyerMesuresSurServeur(float t, float h, float hic)
{
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) 
    {
      HTTPClient http;
      Serial.println("http://dev.astallia.com/tests/arduino/write_sonde.php?temp="+String(t, 1)+"&hygro="+String(h, 1)+"&heat=" + String(hic, 1));
      http.begin("http://dev.astallia.com/tests/arduino/write_sonde.php?temp="+String(t, 1)+"&hygro="+String(h, 1)+"&heat=" + String(hic, 1)); 

      // start connection and send HTTP header
      int httpCode = http.GET();
      if(httpCode) 
      {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if(httpCode == 200) 
        {
            String payload = http.getString();
            Serial.println(payload);
 
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("Envoi OK");
            delay(3000);
            return true;
        }
        else
        {
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Erreur");
          lcd.setCursor(0, 1);
          lcd.print("Code : " + String(httpCode, 1) );
          delay(3000);
          return false;
        }
      } 
      else 
      {
        Serial.print("[HTTP] GET... failed, no connection or no HTTP server\n");

        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Erreur connexion");
        delay(3000);    
        return false;
      }
    }
    else
    {
      Serial.println("Connexion NOK");     

      
      bool b_retour_addAP = WiFiMulti.addAP("SSID", "PASSWORD");

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Wifi NOK - reconnect = " + String(b_retour_addAP));
      delay(3000);
      return false;
    }
}

/*
void parseRss()
{
  TextFinder  finder( client );

  int temp;
  
    // On cherche l'attribut temp
    if(finder.find("temp=") )
    {
      //On stocke la valeur voulue dans la variable temp
      temp = finder.getValue();
      //On affiche cette valeur à des fins de débuggage
      Serial.print("Temp C:  ");
      Serial.println(temp);
    }
    else{
      Serial.print("Pas de donnees");
    }
}
*/



////////////////////////////////////////////////////////////////////////////////
// SERVEUR WEB
////////////////////////////////////////////////////////////////////////////////



void exportDatasXml()
{
  String message = "";
  message = "<?xml version=\"1.0\"?>";
  message += "<etat>";

  message += "<type>";
  message += "<name>temperature DHT22</name>";
  message += "<valeur>"+String(f_temperatureCourante_DHT22) + " *C"+"</valeur>";
  message += "</type>";

  message += "<type>";
  message += "<name>temperature TMP36</name>";
  message += "<valeur>"+String(f_temperatureCourante_TMP36) + " *C"+"</valeur>";
  message += "</type>";

  message += "<type>";
  message += "<name>hygrometrie</name>";
  message += "<valeur>"+String(f_hygroCourante) + " %"+"</valeur>";
  message += "</type>";
  
  message += "<type>";
  message += "<name>indice_chaleur</name>";
  message += "<valeur>"+String(f_heatCourante) + " *C"+"</valeur>";
  message += "</type>";

  message += "<type>";
  message += "<name>luminosite</name>";
  message += "<valeur>"+String(photocellReading) +"</valeur>";
  message += "</type>";
  
  message += "</etat>";
  server.send(200, "text/xml", message);
}



void handleRoot() {
  //digitalWrite(led, 1);
  digitalWrite(BUILTIN_LED, LOW);

 /* String message = "Params\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
*/
  String messageRetour = "";
  String s_formatRetour = "html"; // Format de sortie par defaut = html
  
  bool b_tmp_b_mesureTemperatureDHT22 = false;
  bool b_tmp_b_mesureTemperatureTMP36 = false;
  bool b_tmp_b_mesureHeatIndex = false;
  bool b_tmp_b_mesureHygrometrie = false;
  bool b_tmp_b_mesureLuminosite = false;
  bool b_tmp_b_autoRefreshHtml = false;

  bool b_tmp_b_etatSortieRelais1 = false;
  
  // Parcours et traitement des variables passees en GET
  for (uint8_t i=0; i<server.args(); i++)
  {

    // Argument freq = frequence de mesure des capteurs
    if ( server.argName(i) == "freq"  )
    {
      i_frequenceMesuresMs = server.arg(i).toInt();
      messageRetour += "<br><font color=\"green\">Frequence mesures modifi&eacute; = " + server.arg(i) + " ms </font>" ;
    }

    // Argument refresh : si true on maj le serveur avec la derniere mesure
    else if ( server.argName(i) == "refresh" && ( server.arg(i) == "1" || server.arg(i) == "on") )
    {
      bool b_envoi = envoyerMesuresSurServeur(f_temperatureCourante_DHT22, f_hygroCourante, f_heatCourante);
      if ( b_envoi )
        messageRetour += "<br><font color=\"green\">Serveur mis a jour OK</font>";
      else
        messageRetour += "<br><font color=\"red\">Serveur mis a jour NOK !!</font>";
    }

    else if ( server.argName(i) == "b_mesureTemperatureDHT22" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_mesureTemperatureDHT22 = true;
    else if ( server.argName(i) == "b_mesureTemperatureTMP36" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_mesureTemperatureTMP36 = true;
    else if ( server.argName(i) == "b_mesureHeatIndex" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_mesureHeatIndex = true;
    else if ( server.argName(i) == "b_mesureHygrometrie" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_mesureHygrometrie = true;
    else if ( server.argName(i) == "b_mesureLuminosite" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_mesureLuminosite = true;
    else if ( server.argName(i) == "b_autoRefreshHtml" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_autoRefreshHtml = true;
    else if ( server.argName(i) == "b_etatSortieRelais1" && ( server.arg(i) == "1" || server.arg(i) == "on") )
      b_tmp_b_etatSortieRelais1 = true;

    else if ( server.argName(i) == "i_varCourante" )
    {
      bool b_majLCD = (i_varCourante != server.arg(i).toInt());
      i_varCourante = server.arg(i).toInt();

      // Affiche la valeur de la variable courante
      if ( b_majLCD ) afficheValeurLCD(i_varCourante);
    }
    else if( server.argName(i) == "etat_ch_1" )
    {
      s_formatRetour = "xml";
    }

  }// FIN Parcours et traitement des variables passees en GET

  if ( server.args() > 0)
  {
    b_mesureTemperatureDHT22 = b_tmp_b_mesureTemperatureDHT22;
    b_mesureTemperatureTMP36 = b_tmp_b_mesureTemperatureTMP36;
    b_mesureHeatIndex = b_tmp_b_mesureHeatIndex;
    b_mesureHygrometrie = b_tmp_b_mesureHygrometrie;
    b_mesureLuminosite = b_tmp_b_mesureLuminosite;
    b_autoRefreshHtml = b_tmp_b_autoRefreshHtml;

    if ( b_etatSortieRelais1 != b_tmp_b_etatSortieRelais1 )
    {
      if ( b_tmp_b_etatSortieRelais1 )
        digitalWrite(RELAIS1PIN, HIGH);
      else
        digitalWrite(RELAIS1PIN, LOW);
    }
    
    b_etatSortieRelais1 = b_tmp_b_etatSortieRelais1;
  }

  String message = "";

  // GENERATION DE LA PAGE WEB D'ADMIN
  if ( s_formatRetour == "html" )
  {
    message = "<html>\
                      <head>\
                        <title>ADMIN ESP8266</title>";
  
    if ( b_autoRefreshHtml )
    {
      int i_freqSeconde = i_frequenceMesuresMs / 1000;
      if ( i_freqSeconde > 30  )
        message += "<meta http-equiv='refresh' content='"+ String(i_freqSeconde) +"'/>";
      else
        message += "<meta http-equiv='refresh' content='30'/>";
    } 
  
    message += "      <style>\
                          body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
                        </style>\
                      </head>\
                        <body>\
                          <h1>Admin Station Meteo Arduino</h1>\
                          <form action=\"\" method=\"POST\">";
  
    // DEBUG
    /*message += "DEBUG : " + String(server.args()) + " args";
    for (uint8_t i=0; i<server.args(); i++){
      message += "<br> -> " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    message += "<br><br>";*/
  
    if ( b_mesureTemperatureDHT22)
      message += "<br>Temperature DHT22 = " + String(f_temperatureCourante_DHT22, 1) + " *C" ;
    if ( b_mesureHeatIndex)
      message += "<br>Indice de chaleur DHT22 = " + String(f_heatCourante, 1) + " *C" ;
    if ( b_mesureTemperatureTMP36)
      message += "<br>Temperature TMP36 = " + String(f_temperatureCourante_TMP36, 1) + " *C" ;
    if ( b_mesureHygrometrie)
      message += "<br>Hygrometrie = " + String(f_hygroCourante, 1) + " %" ;
    if ( b_mesureLuminosite)
      message += "<br>Luminosite = " + String(photocellReading) + "" ;
  
    message += "<br>";
    message += "<br>Valeurs Mesurees :";
  
    message += "<input type=\"hidden\" name=\"b_mesureTemperatureDHT22\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_mesureTemperatureTMP36\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_mesureHeatIndex\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_mesureHygrometrie\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_mesureLuminosite\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_autoRefreshHtml\" value=\"0\" />";
    message += "<input type=\"hidden\" name=\"b_etatSortieRelais1\" value=\"0\" />";
      
    if ( b_mesureTemperatureDHT22 )
      message += "<br>Temperature DHT22 : <input type=\"checkbox\" name=\"b_mesureTemperatureDHT22\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Temperature DHT22 : <input type=\"checkbox\" name=\"b_mesureTemperatureDHT22\" value=\"1\">";

    if ( b_mesureTemperatureTMP36 )
      message += "<br>Temperature TMP36 : <input type=\"checkbox\" name=\"b_mesureTemperatureTMP36\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Temperature TMP36 : <input type=\"checkbox\" name=\"b_mesureTemperatureTMP36\" value=\"1\">";
  
    if ( b_mesureHeatIndex )
      message += "<br>Indice de chaleur : <input type=\"checkbox\" name=\"b_mesureHeatIndex\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Indice de chaleur : <input type=\"checkbox\" name=\"b_mesureHeatIndex\" value=\"1\">";
      
    if ( b_mesureHygrometrie )
      message += "<br>Hygrometrie : <input type=\"checkbox\" name=\"b_mesureHygrometrie\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Hygrometrie : <input type=\"checkbox\" name=\"b_mesureHygrometrie\" value=\"1\">";
  
    if ( b_mesureLuminosite )
      message += "<br>Luminosite : <input type=\"checkbox\" name=\"b_mesureLuminosite\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Luminosite : <input type=\"checkbox\" name=\"b_mesureLuminosite\" value=\"1\">";
  
    message += "<br>";
    message += "<br>Variable affichee sur le LCD : ";
    message += "<select name=\"i_varCourante\">";
  
    if (i_varCourante==VAR_TEMP_DHT22)
      message += "<option value=\""+String(VAR_TEMP_DHT22)+"\" selected>Temperature DHT22</option>";
    else
      message += "<option value=\""+String(VAR_TEMP_DHT22)+"\">Temperature DHT22</option>";

    if (i_varCourante==VAR_TEMP_TMP36)
      message += "<option value=\""+String(VAR_TEMP_TMP36)+"\" selected>Temperature TMP36</option>";
    else
      message += "<option value=\""+String(VAR_TEMP_TMP36)+"\">Temperature TMP36</option>";
  
    if (i_varCourante==VAR_HEAT)
      message += "<option value=\""+String(VAR_HEAT)+"\" selected>Indice de chaleur</option>";
    else
      message += "<option value=\""+String(VAR_HEAT)+"\">Indice de chaleur</option>";
      
    if (i_varCourante==VAR_HYGRO)
      message += "<option value=\""+String(VAR_HYGRO)+"\" selected>Hygrometrie</option>";
    else
      message += "<option value=\""+String(VAR_HYGRO)+"\">Hygrometrie</option>";   
  
    if (i_varCourante==VAR_LUMINOSITE)
      message += "<option value=\""+String(VAR_LUMINOSITE)+"\" selected>Luminosite</option>";
    else
      message += "<option value=\""+String(VAR_LUMINOSITE)+"\">Luminosite</option>";
      
    message += "</select>";
  
    message += "<br>";
    message += "<br>Frequence (ms) : <input type=\"text\" name=\"freq\" value=\""+String(i_frequenceMesuresMs)+"\">";
  
    message += "<br>";
    if ( b_etatSortieRelais1 )
      message += "<br>Etat relais 1 (D3) : <input type=\"checkbox\" name=\"b_etatSortieRelais1\" value=\"1\" checked=\"1\">";
    else
      message += "<br>Etat relais 1 (D3) : <input type=\"checkbox\" name=\"b_etatSortieRelais1\" value=\"1\">";
   
    message += "<br><br>Mettre a jour le serveur : <input type=\"checkbox\" name=\"refresh\">";
    
    if ( b_autoRefreshHtml)
      message += "<br><br>AutoRefresh page : <input type=\"checkbox\" name=\"b_autoRefreshHtml\" value=\"1\" checked=\"1\">";
    else
      message += "<br><br>AutoRefresh page : <input type=\"checkbox\" name=\"b_autoRefreshHtml\" value=\"1\">";
  
    
    message += "<br><input type=\"submit\" value=\"OK\">";
    message += "</form>";
    
    message += "<br>" + messageRetour;
    
    message += "</body></html>";
    
    server.send(200, "text/html", message);

  }// FIN GENERATION DE LA PAGE WEB D'ADMIN
  else if ( s_formatRetour == "xml" )
  {
      exportDatasXml();
  }
  
  //digitalWrite(led, 0); 
  digitalWrite(BUILTIN_LED, HIGH);
}


String getValeurAttribut(String _s_attribut)
{
  for (uint8_t i=0; i<server.args(); i++){
    if ( server.argName(i) == _s_attribut )
      return server.arg(i);
  }
  return "";
}






void handleNotFound(){
  //digitalWrite(led, 1);
  digitalWrite(BUILTIN_LED, LOW);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  //digitalWrite(led, 0);
  digitalWrite(BUILTIN_LED, HIGH);
}


////////////////////////////////////////////////////////////////////////////////




void setup() 
{  
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output

  pinMode(slaveSelect, OUTPUT);  // D8 = SS SPI
  
  // initialize digital pin as an output.
  pinMode(RELAIS1PIN, OUTPUT);
  
  // Debugging output
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);

  // Print a message to the LCD. We track how long it takes since
  // this library has been optimized a bit and we're proud of it :)
  int time = millis();
  lcd.print("Hello !");
  time = millis() - time;
  Serial.print("Took "); Serial.print(time); Serial.println(" ms");
  lcd.setBacklight(WHITE);

  dht.begin();

  bool b_retour_addAP = WiFiMulti.addAP("FREEZ", "2ce0ma0ju9myBoca");
  Serial.print("Connexion au point d acces : " + String(b_retour_addAP));
  i_varCourante = VAR_TEMP_DHT22;

  // Initialisation du bus SPI
  SPI.begin();
  // Configuration du mode d'envoi des données :
  SPI.setBitOrder(MSBFIRST);  // bit le plus à gauche en premier
  SPI.setDataMode(SPI_MODE0); // Mode 0 : données capturées sur front montant d'horloge et transmises sur front descendant 

  digitalWrite(slaveSelect, LOW);        // Cycle the ADC CS pin as per datasheet
  digitalWrite(slaveSelect, HIGH);

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", [](){
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/etat_ch_1", exportDatasXml);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

  timeDebut = millis();

  lcd.clear();
  lcd.setCursor(0,0);
  if ( b_retour_addAP )
    lcd.print("Connexion OK");
  else
    lcd.print("Connexion NOK !");
  delay(1000);
}







void loop() 
{

  server.handleClient();
  
  // Lecture de l'etat des boutons
  uint8_t buttons = lcd.readButtons();

  if (buttons) 
  {
    lcd.clear();
    lcd.setCursor(0,0);
    
    if (buttons & BUTTON_UP) 
    {
      if ( i_varCourante < 5)
        i_varCourante++;
      else
        i_varCourante=1;

      // Affiche la valeur de la variable courante
      afficheValeurLCD(i_varCourante);
    }
    if (buttons & BUTTON_DOWN) 
    {
      if ( i_varCourante > 1)
        i_varCourante--;
      else
        i_varCourante=5;

      // Affiche la valeur de la variable courante
      afficheValeurLCD(i_varCourante);
    }
    if (buttons & BUTTON_LEFT) 
    {
    
    }
    if (buttons & BUTTON_RIGHT) 
    {
       envoyerMesuresSurServeur(f_temperatureCourante_DHT22, f_hygroCourante, f_heatCourante);
    }
    if (buttons & BUTTON_SELECT) 
    {
      if ( i_couleurCourante < 7)
        i_couleurCourante++;
      else
        i_couleurCourante=1;
      lcd.setBacklight(i_couleurCourante);
    }
  }


  timeFin = millis();

  if ( ( timeFin - timeDebut ) > i_frequenceMesuresMs )
  {
    // Mesure de la temperature
    if ( b_mesureTemperatureDHT22 )
      mesureValeur(VAR_TEMP_DHT22);
    // Mesure de la temperature
    if ( b_mesureTemperatureTMP36 )
      mesureValeur(VAR_TEMP_TMP36);
    // Mesure de l'hygrometrie
    if ( b_mesureHygrometrie )
      mesureValeur(VAR_HYGRO);
    // Mesure de index heat
    if ( b_mesureHeatIndex )
      mesureValeur(VAR_HEAT);
    // Mesure de la luminosite
    if ( b_mesureLuminosite )
      mesureValeur(VAR_LUMINOSITE);


/*
delay(250);


  double vRef        = 4.78;
  int    adc_reading0 = 0;
 
  adc_reading0        = adc_single_channel_read (adc_single_ch0);
 
  Serial.print       ("ADC Ch ");
  Serial.print       (adc_single_ch0 & 0x07);

  Serial.print       (" Read 0: ");
  Serial.println     (String(adc_reading0,1));
  
  Serial.print       (" Voltage 0: ");
  Serial.println     ((adc_reading0 * vRef) / 1024, 2);



int    adc_reading1 = 0;
 
  adc_reading1        = adc_single_channel_read (adc_single_ch1);
 
  Serial.print       ("ADC Ch ");
  Serial.print       (adc_single_ch1 & 0x07);

  Serial.print       (" Read 1: ");
  Serial.println     (String(adc_reading1,1));
  
  Serial.print       (" Voltage 1: ");
  Serial.println     ((adc_reading1 * vRef) / 1024, 2);




int    adc_reading2 = 0;
 
  adc_reading2        = adc_single_channel_read (adc_single_ch2);
 
  Serial.print       ("ADC Ch ");
  Serial.print       (adc_single_ch2 & 0x07);

  Serial.print       (" Read 2: ");
  Serial.println     (String(adc_reading2,1));
  
  Serial.print       (" Voltage 2: ");
  Serial.println     ((adc_reading2 * vRef) / 1024, 2);

  delay(250);

*/



    if ( s_sablier == "#" ) s_sablier = "*";
    else if ( s_sablier == "*" ) s_sablier = "#";
  
    // Affiche la valeur de la variable courante
    afficheValeurLCD(i_varCourante);

    timeDebut = millis();
    timeFin = millis();
  }
  
  //delay(i_frequenceMesuresMs);
  delay(500);

}
